package com.praxis.TAC.QRRegistration;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    Bundle current_event;
    boolean isBundlePassed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(ScanActivity.this);
        mScannerView.setResultHandler(this);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(mScannerView);

        if(Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if(Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            getSupportActionBar().hide();
            decorView.setSystemUiVisibility(uiOptions);
        }

        current_event = new Bundle();
        Intent intent = getIntent();
        if(intent.getBundleExtra("curr_bundle")!=null){
            isBundlePassed=true;
            current_event = intent.getBundleExtra("curr_bundle");
            Toast.makeText(this, "Event:" + current_event.getString("event_name"),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mScannerView.setResultHandler(ScanActivity.this);
        mScannerView.startCamera();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus)
        {
            if(Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
                View v = this.getWindow().getDecorView();
                v.setSystemUiVisibility(View.GONE);
            } else if(Build.VERSION.SDK_INT >= 19) {
                //for new api versions.
                View decorView = getWindow().getDecorView();
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                getSupportActionBar().hide();
                decorView.setSystemUiVisibility(uiOptions);
            }
        }
    }

    @Override
    public void handleResult(Result result) {

        if(isBundlePassed){
            //Register for onDayRegistration
            JSONObject req = new JSONObject();
            try {
                JSONObject body = new JSONObject();
                req.put("method", "POST");
                req.put("url", "onDayRegistration");
                body.put("event_id", current_event.getString("event_id"));
                body.put("qr_id", result.getText());
                req.put("body", body);
                Toast.makeText(ScanActivity.this ,current_event.getString("event_id") + ":" +  result.getText() , Toast.LENGTH_SHORT).show();

                new APIMiddleware(new APIMiddleware.TaskListener() {
                    ProgressDialog progressDialog;
                    boolean success=false;
                    @Override
                    public void onTaskBegin() {
                        progressDialog = new ProgressDialog(ScanActivity.this);
                        progressDialog.setCancelable(true);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.setMessage("Registering for event...");
                        progressDialog.setProgress(0);
                        progressDialog.setMax(100);
                        progressDialog.show();
                        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ScanActivity.this);
                                if(success){
                                    builder.setTitle("Success");
                                    builder.setMessage("Successfully registered for event");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            mScannerView.resumeCameraPreview(ScanActivity.this);
                                        }
                                    });
                                }else{
                                    builder.setTitle("Failure");
                                    builder.setMessage("Something went wrong while registering, Scan QR code again");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            mScannerView.resumeCameraPreview(ScanActivity.this);
                                        }
                                    });
                                }

                                AlertDialog dialog1 = builder.create();
                                dialog1.setCanceledOnTouchOutside(true);
                                dialog1.show();
                            }
                        });
                    }

                    @Override
                    public void onTaskCompleted(JSONObject response) {
                        Toast.makeText(ScanActivity.this , "onTaskCompleted", Toast.LENGTH_SHORT).show();
                        if(response!=null)
                        {
                            //Toast.makeText(ScanActivity.this , "Response not null", Toast.LENGTH_SHORT).show();
                            String code = "";
                            try {
                                code = response.get("code").toString();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //Toast.makeText(ScanActivity.this, "Code:"+code,Toast.LENGTH_SHORT).show();
                            AlertDialog.Builder builder = new AlertDialog.Builder(ScanActivity.this);
                            if(code.equals("200") || code.equals("201")){
                                success=true;
                                progressDialog.cancel();
                            }else{
                                success=false;
                                progressDialog.cancel();
                            }
                            AlertDialog dialog1 = builder.create();
                            dialog1.setCanceledOnTouchOutside(true);
                            dialog1.show();

                        }else{
                            //empty response
                            //Toast.makeText(ScanActivity.this, "Response is null", Toast.LENGTH_SHORT).show();
                            AlertDialog.Builder builder = new AlertDialog.Builder(ScanActivity.this);
                            builder.setTitle("Error");
                            builder.setMessage("Something went wrong");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mScannerView.resumeCameraPreview(ScanActivity.this);
                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.setCanceledOnTouchOutside(true);
                            dialog.show();
                        }
                    }
                }).execute(req);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            //Register for qrid
            Intent data = new Intent();
            data.putExtra("qr" , result.getText());
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    protected void onPause() {
        mScannerView.stopCamera();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.startCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mScannerView.stopCamera();
    }
}
