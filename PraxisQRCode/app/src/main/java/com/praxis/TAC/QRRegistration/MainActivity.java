package com.praxis.TAC.QRRegistration;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.praxis.TAC.QRRegistration.RealmFiles.Events;
import com.praxis.TAC.QRRegistration.models.Event;
import com.praxis.TAC.QRRegistration.models.EventsResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity{

    public static final String PREFS="MyPrefs";
    public static final String PREFS_KEY_NAME="Event_name";
    public static final String PREFS_KEY_RESULT="Scan_result";
    AlertDialog globaldialog;
    boolean registerclick = false;

    JSONObject request;

    String selected;

    ProgressDialog progressDialog;
    boolean success=false;

    boolean camera_permission=false;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        request = new JSONObject();
        try{
            request.put("method", "GET");
            request.put("url", "events");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No internet connection");
        builder.setMessage("Please check your internet connection");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        globaldialog = builder.create();
        globaldialog.setCanceledOnTouchOutside(true);

    }

    class Run implements Runnable {
        boolean success = false;
        @Override
        public void run() {
            try{
                URL url = new URL("https://google.com");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(10000);
                connection.connect();
                success = connection.getResponseCode() == 200;

                if(!success){
                    showNoNetDialog();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(ContextCompat.checkSelfPermission(MainActivity.this,Manifest.permission.CAMERA)==PackageManager.PERMISSION_GRANTED)
            camera_permission=true;
        else
            camera_permission=false;
    }

    void showNoNetDialog() {
        globaldialog.show();
    }

    public void onClickRegister(View view) {
        registerclick = true;
        if(camera_permission)
        {
                    Intent registerIntent = new Intent(MainActivity.this, RegisterActivity.class);
                    startActivity(registerIntent);
        }else {
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA},0);
        }
    }

    public void onClickEventScan(View view) {
        registerclick = false;
        if(camera_permission) {
            SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
            boolean sync = prefs.getBoolean("synced", false);
            if (sync) {
                    Intent intent = new Intent(MainActivity.this, EventsActivity.class);
                    startActivity(intent);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Unsyncned");
                builder.setMessage("You have not synced events yet");
                builder.setPositiveButton("Sync", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestFromServer(request);
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alertdialog = builder.create();
                alertdialog.setCanceledOnTouchOutside(true);
                alertdialog.show();
            }
        }else {
            ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA},0);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            AlertDialog dialog;
            if(resultCode==RESULT_OK){

                /*******Code for sending result to remote db****************/
                SharedPreferences preferences = getSharedPreferences(PREFS,MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                editor.putString(PREFS_KEY_NAME,selected);
                editor.putString(PREFS_KEY_RESULT,data.getStringExtra("Result"));
                editor.commit();

                //Send data to remote db

                /***********************************************************/

                builder.setTitle("Success");
                builder.setMessage("Successfully registered for " + selected);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.show();
                dialog.setCanceledOnTouchOutside(true);
            }else if(resultCode==RESULT_CANCELED){
                builder.setTitle("Failure");
                builder.setMessage("Something went wrong");
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.show();
                dialog.setCanceledOnTouchOutside(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sync_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.sync)
        {
            syncEvents();
        }/*else if(item.getItemId() == R.id.delete){

            deleteAll();
        }*/

        return super.onOptionsItemSelected(item);
    }

    void deleteAll(){
        RealmConfiguration configuration = new RealmConfiguration.Builder(MainActivity.this)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm realm = Realm.getInstance(configuration);
        RealmResults<Events> res = realm.where(Events.class)
                .findAllSorted("event_id", Sort.ASCENDING);

        realm.beginTransaction();
        res.deleteAllFromRealm();
        realm.commitTransaction();

        SharedPreferences prefs = getSharedPreferences("PREFS" , MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putBoolean("synced",false);
        editor.commit();
    }

    private void syncEvents() {

        RealmConfiguration config = new RealmConfiguration
                .Builder(MainActivity.this)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm realm = Realm.getInstance(config);
        if(!realm.isEmpty())
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Synced");
            builder.setMessage("Events are already synced");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setPositiveButton("Resync", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //deleteAll();
                    requestFromServer(request);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(true);
            dialog.show();
        }else {
                requestFromServer(request);
        }
    }

    void requestFromServer(JSONObject req)
    {
        new APIMiddleware(new APIMiddleware.TaskListener() {

            @Override
            public void onTaskBegin() {
                progressDialog = new ProgressDialog(MainActivity.this);
                progressDialog.setCancelable(true);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Syncing");
                progressDialog.setProgress(0);
                progressDialog.setMax(100);
                progressDialog.show();
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                    @Override
                    public void onCancel(DialogInterface dialog) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        if(success)
                        {
                            builder.setTitle("Success");
                            builder.setMessage("Syncing successful");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }else{
                            builder.setTitle("Failure");
                            builder.setMessage("Syncing failed");
                            builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    requestFromServer(request);
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }

                        AlertDialog alertdialog = builder.create();
                        alertdialog.setCanceledOnTouchOutside(true);
                        alertdialog.show();
                    }
                });
            }

            @Override
            public void onTaskCompleted(JSONObject response) {
                syncAndStore(response);
                progressDialog.cancel();
            }


        }).execute(req);
    }

    void syncAndStore(JSONObject response){
        if (response != null) {
            //try {
            //Checking status code , if 200 , then proper response
            String code="";
            try {
                code = response.get("code").toString();
                Toast.makeText(MainActivity.this ,"Code:"+code, Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (code.equals("200")) {
                deleteAll();
                String resjson = response.toString();
                Gson gson = new GsonBuilder().create();
                EventsResponse eventsResponse = gson.fromJson(resjson, EventsResponse.class);
                List<Event> list = eventsResponse.getData();

                RealmConfiguration configuration = new RealmConfiguration
                        .Builder(MainActivity.this)
                        .deleteRealmIfMigrationNeeded()
                        .build();
                Realm realm = Realm.getInstance(configuration);

                RealmResults<Events> res = realm.where(Events.class)
                        .findAllSorted("event_id", Sort.ASCENDING);
                for (int i = 0; i < list.size(); i++) {
                    realm.beginTransaction();

                    Events curr = realm.createObject(Events.class);
                    curr.setEvent_id(list.get(i).getEvent_id());
                    curr.setCategory(list.get(i).getCategory());
                    curr.setEvent_name(list.get(i).getEvent_name());
                    curr.setCost(list.get(i).getCost());
                    curr.setPrize(list.get(i).getPrize());
                    curr.setTeams_of(list.get(i).getTeams_of());
                    curr.setDescription(list.get(i).getDescription());
                    curr.setDate(list.get(i).getDate());

                    realm.commitTransaction();
                    success=true;

                    SharedPreferences prefs = getSharedPreferences("PREFS" , MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();

                    editor.putBoolean("synced",true);
                    editor.commit();

                    progressDialog.cancel();
                }
            } else {
                //Error occured
                success=false;
                progressDialog.cancel();
            }
        }//if response check status code
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Error");
            builder.setMessage("Something went wrong");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(true);
            dialog.show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 0:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    //Intent scanIntent = new Intent(MainActivity.this,ScanActivity.class);
                    //startActivity(scanIntent);
                    SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
                    boolean sync = prefs.getBoolean("synced", false);
                    if (sync) {
                        Intent intent;
                        if(registerclick) {
                            intent = new Intent(MainActivity.this, RegisterActivity.class);
                        }else{
                            intent = new Intent(MainActivity.this, EventsActivity.class);
                        }
                        startActivity(intent);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle("Unsyncned");
                        builder.setMessage("You have not synced events yet");
                        builder.setPositiveButton("Sync", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestFromServer(request);
                            }
                        });

                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alertdialog = builder.create();
                        alertdialog.setCanceledOnTouchOutside(true);
                        alertdialog.show();
                    }
                }
        }
    }
}
