package com.praxis.TAC.QRRegistration;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    private static final int REQUEST_CODE=0;
    EditText name;
    EditText phone;
    EditText college;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onClickScanAndSubmit(View view) {

        name = (EditText) findViewById(R.id.name);
        phone = (EditText)findViewById(R.id.phone);
        college = (EditText)findViewById(R.id.college);

        boolean scan=true;

        TextView name_error = (TextView)findViewById(R.id.empty_name_error);
        TextView phone_error = (TextView)findViewById(R.id.empty_phone_error);
        TextView college_error = (TextView)findViewById(R.id.empty_college_error);

        if(name.getEditableText().toString().equals(""))
        {
            name_error.setVisibility(View.VISIBLE);
            scan=false;
        }else
        {
            name_error.setVisibility(View.INVISIBLE);
        }

        if(phone.getEditableText().toString().equals(""))
        {
            phone_error.setText("*phone cannot be empty");
            phone_error.setVisibility(View.VISIBLE);
            scan=false;
        }else{
            phone_error.setVisibility(View.INVISIBLE);
        }

        if(phone.getEditableText().toString().length()!=10)
        {
            phone_error.setText("*not a 10-digit phone number");
            phone_error.setVisibility(View.VISIBLE);
            scan=false;
        }else{
            phone_error.setVisibility(View.INVISIBLE);
        }


        if(college.getEditableText().toString().equals(""))
        {
            college_error.setVisibility(View.VISIBLE);
            scan=false;
        }else{
            college_error.setVisibility(View.INVISIBLE);
        }

        if(scan) {
            Intent scanIntent = new Intent(RegisterActivity.this, ScanActivity.class);
            startActivityForResult(scanIntent, REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE)
        {
            if(resultCode==RESULT_OK){

                //Code for sending data to remote DB
                JSONObject req = new JSONObject();
                //data.get("qr") contains the qr code in text
                try {
                    req.put("method", "PUT");
                    req.put("url", "qrid/"+data.getStringExtra("qr"));
                    JSONObject body = new JSONObject();
                    body.put("name", name.getEditableText().toString());
                    body.put("contact", phone.getEditableText().toString());
                    body.put("college", college.getEditableText().toString());
                    body.put("status", 1);
                    req.put("body", body);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                new APIMiddleware(new APIMiddleware.TaskListener() {
                    @Override
                    public void onTaskBegin() {

                    }

                    @Override
                    public void onTaskCompleted(JSONObject response) {
                        if(response!=null){
                        String code = "";
                        try {
                            code = response.getString("code");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                            Toast.makeText(RegisterActivity.this,"Code:" + code, Toast.LENGTH_SHORT).show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        if (code.equals("200") || code.equals("201")) {
                            //success
                            builder.setTitle("Success");
                            builder.setMessage("Registration successful");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        } else {
                            //failure
                            builder.setTitle("Failure");
                            builder.setMessage("Registration failed");
                            builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }

                        AlertDialog dialog = builder.create();
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                    }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                            builder.setTitle("Error");
                            builder.setMessage("Something went wrong");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.setCanceledOnTouchOutside(true);
                            dialog.show();
                    }
                    }
                }).execute(req);


                Toast.makeText(RegisterActivity.this,data.getStringExtra("qr"),Toast.LENGTH_SHORT).show();
                name.setText("");
                phone.setText("");
                college.setText("");
                name.setFocusable(true);

            }else if(resultCode==RESULT_CANCELED){
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setTitle("Error");
                builder.setMessage("Something went wrong");
                builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
            }
        }
    }

}
