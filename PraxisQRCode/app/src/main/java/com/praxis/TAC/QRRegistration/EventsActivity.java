package com.praxis.TAC.QRRegistration;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.praxis.TAC.QRRegistration.RealmFiles.Events;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

public class EventsActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {


    RecyclerView recyclerView;
    MyAdapter adapter;
    Realm realm;
    RealmResults<Events> realmResults;
    List<Events> listmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(EventsActivity.this));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        RealmConfiguration configuration = new RealmConfiguration
                .Builder(EventsActivity.this)
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(configuration);

        realmResults = realm
                .where(Events.class)
                .findAllSorted("event_id", Sort.ASCENDING);

        listmodel = new ArrayList<Events>();
        for(int i=0;i<realmResults.size();i++)
        {
            listmodel.add(realmResults.get(i));
        }

        adapter = new MyAdapter(EventsActivity.this , listmodel);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search,menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(EventsActivity.this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<Events> filteredlist = filter(listmodel , newText);
        adapter.setFilter(filteredlist);
        return true;
    }

    List<Events> filter(List<Events> res , String query){
        query = query.toLowerCase();
        List<Events> filterdModelList = new ArrayList<Events>();
        for(Events event : res){
            String text = event.getEvent_name().toLowerCase();
            if(text.contains(query)){
                filterdModelList.add(event);
            }
        }
        return filterdModelList;
    }
}
