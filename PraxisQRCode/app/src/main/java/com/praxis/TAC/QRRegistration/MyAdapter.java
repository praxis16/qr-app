package com.praxis.TAC.QRRegistration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.praxis.TAC.QRRegistration.RealmFiles.Events;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jayesh Saita on 18-Jul-16.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    List<Events> result;
    Context context;
    LayoutInflater inflater;

    MyAdapter(Context context, List<Events> res)
    {
        this.context = context;
        result = res;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.custom_row,parent,false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(result.get(position).getEvent_name());
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.event_name_textview);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Bundle bundle = new Bundle();
            Events curr = result.get(getAdapterPosition());
            bundle.putString("event_id" , curr.getEvent_id());
            bundle.putString("category", curr.getCategory());
            bundle.putString("event_name", curr.getEvent_name());
            bundle.putString("cost", curr.getCost());
            bundle.putString("prize", curr.getPrize());
            bundle.putString("teams_of", curr.getTeams_of());
            bundle.putString("description", curr.getDescription());
            bundle.putString("date", curr.getDate());

            Intent intent = new Intent(context , ScanActivity.class);
            intent.putExtra("curr_bundle" , bundle);
            context.startActivity(intent);
        }
    }

    public void setFilter(List<Events> res){
        result = new ArrayList<Events>();
        result.addAll(res);
        notifyDataSetChanged();
    }
}
